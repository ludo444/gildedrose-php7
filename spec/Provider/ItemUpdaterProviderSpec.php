<?php

namespace spec\App\Provider;

use App\Item;
use App\Provider\ItemUpdaterProvider;
use App\Updater\ItemUpdaterInterface;
use PhpSpec\ObjectBehavior;

class ItemUpdaterProviderSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ItemUpdaterProvider::class);
    }

    function it_gets_updater_for_aged_brie_item()
    {
        $item = new Item('Aged Brie', 1, 1);
        $this->getUpdater($item)->shouldReturnAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_gets_updater_for_backstage_pass_item()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 1, 1);
        $this->getUpdater($item)->shouldReturnAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_gets_updater_for_conjured_item()
    {
        $item = new Item('Conjured', 1, 1);
        $this->getUpdater($item)->shouldReturnAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_gets_updater_for_sulfuras_item()
    {
        $item = new Item('Sulfuras, Hand of Ragnaros', 1, 1);
        $this->getUpdater($item)->shouldReturnAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_gets_updater_for_normal_item()
    {
        $item = new Item('normal', 1, 1);
        $this->getUpdater($item)->shouldReturnAnInstanceOf(ItemUpdaterInterface::class);
    }
}
