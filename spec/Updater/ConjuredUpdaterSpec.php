<?php

namespace spec\App\Updater;

use App\Item;
use App\Updater\ConjuredUpdater;
use App\Updater\ItemUpdaterInterface;
use PhpSpec\ObjectBehavior;

class ConjuredUpdaterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ConjuredUpdater::class);
    }

    function it_is_an_item_updater()
    {
        $this->shouldBeAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_supports_only_conjured_item()
    {
        $this->supportsItem(new Item('Conjured', 1, 1))->shouldReturn(true);
        $this->supportsItem(new Item('any', 1, 1))->shouldReturn(false);
    }

    function it_updates_item_by_decreasing_quality_by_2_and_sell_in()
    {
        $item = new Item('Conjured', 10, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(8);
    }

    function it_updates_item_by_decreasing_quality_by_4_on_sell_in_day()
    {
        $item = new Item('Conjured', 1, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(0);
        expect($item->quality)->shouldBe(6);
    }

    function it_doesnt_allow_negative_quality()
    {
        $item = new Item('Conjured', 10, -1);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(0);
    }

    function it_doesnt_allow_quality_over_50()
    {
        $item = new Item('Conjured', 10, 60);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(50);
    }
}
