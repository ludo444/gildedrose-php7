<?php

namespace spec\App\Updater;

use App\Item;
use App\Updater\ItemUpdaterInterface;
use App\Updater\SulfurasUpdater;
use PhpSpec\ObjectBehavior;

class SulfurasUpdaterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SulfurasUpdater::class);
    }

    function it_is_an_item_updater()
    {
        $this->shouldBeAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_supports_only_sulfuras_item()
    {
        $this->supportsItem(new Item('Sulfuras, Hand of Ragnaros', 1, 1))->shouldReturn(true);
        $this->supportsItem(new Item('any', 1, 1))->shouldReturn(false);
    }

    function it_sets_quality_to_80_and_doesnt_update_sell_in()
    {
        $item = new Item('Sulfuras, Hand of Ragnaros', 10, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(10);
        expect($item->quality)->shouldBe(80);
    }
}
