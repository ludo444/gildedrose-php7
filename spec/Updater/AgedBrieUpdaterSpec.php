<?php

namespace spec\App\Updater;

use App\Item;
use App\Updater\AgedBrieUpdater;
use App\Updater\ItemUpdaterInterface;
use PhpSpec\ObjectBehavior;

class AgedBrieUpdaterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(AgedBrieUpdater::class);
    }

    function it_is_an_item_updater()
    {
        $this->shouldBeAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_supports_only_aged_brie_item()
    {
        $this->supportsItem(new Item('Aged Brie', 1, 1))->shouldReturn(true);
        $this->supportsItem(new Item('any', 1, 1))->shouldReturn(false);
    }

    function it_updates_item_by_increasing_quality_and_decreasing_sell_in()
    {
        $item = new Item('Aged Brie', 10, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(11);
    }

    function it_updates_item_by_increasing_quality_by_double_on_sell_in_day()
    {
        $item = new Item('Aged Brie', 1, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(0);
        expect($item->quality)->shouldBe(12);
    }

    function it_doesnt_allow_negative_quality()
    {
        $item = new Item('Aged Brie', 10, -10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(0);
    }

    function it_doesnt_allow_quality_over_50()
    {
        $item = new Item('Aged Brie', 10, 60);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(50);
    }
}
