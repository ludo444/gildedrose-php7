<?php

namespace spec\App\Updater;

use App\Item;
use App\Updater\BackstagePassUpdater;
use App\Updater\ItemUpdaterInterface;
use PhpSpec\ObjectBehavior;

class BackstagePassUpdaterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BackstagePassUpdater::class);
    }

    function it_is_an_item_updater()
    {
        $this->shouldBeAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_supports_only_backstage_pass_item()
    {
        $this->supportsItem(new Item('Backstage passes to a TAFKAL80ETC concert', 1, 1))->shouldReturn(true);
        $this->supportsItem(new Item('any', 1, 1))->shouldReturn(false);
    }

    function it_updates_item_by_increasing_quality_and_decreasing_sell_in()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 20, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(19);
        expect($item->quality)->shouldBe(11);
    }

    function it_increases_quality_by_double_after_day_10()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(12);
    }

    function it_increases_quality_by_triple_after_day_5()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 5, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(4);
        expect($item->quality)->shouldBe(13);
    }

    function it_sets_quality_to_0_after_sell_in_day()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 1, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(0);
        expect($item->quality)->shouldBe(0);
    }

    function it_doesnt_allow_negative_quality()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, -10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(0);
    }

    function it_doesnt_allow_quality_over_50()
    {
        $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, 60);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(50);
    }
}
