<?php

namespace spec\App\Updater;

use App\Item;
use App\Updater\ItemUpdater;
use App\Updater\ItemUpdaterInterface;
use PhpSpec\ObjectBehavior;

class ItemUpdaterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ItemUpdater::class);
    }

    function it_is_an_item_updater()
    {
        $this->shouldBeAnInstanceOf(ItemUpdaterInterface::class);
    }

    function it_supports_any_item()
    {
        $this->supportsItem(new Item('any', 1, 1))->shouldReturn(true);
        $this->supportsItem(new Item('another', 1, 1))->shouldReturn(true);
    }

    function it_updates_item_by_decreasing_quality_and_sell_in()
    {
        $item = new Item('normal', 10, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(9);
    }

    function it_updates_item_by_decreasing_quality_by_double_on_sell_in_day()
    {
        $item = new Item('normal', 1, 10);
        $this->update($item);
        expect($item->sell_in)->shouldBe(0);
        expect($item->quality)->shouldBe(8);
    }

    function it_doesnt_allow_negative_quality()
    {
        $item = new Item('normal', 10, -1);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(0);
    }

    function it_doesnt_allow_quality_over_50()
    {
        $item = new Item('normal', 10, 60);
        $this->update($item);
        expect($item->sell_in)->shouldBe(9);
        expect($item->quality)->shouldBe(50);
    }
}
