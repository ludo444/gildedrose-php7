<?php

namespace spec\App;

use App\GildedRose;
use App\Item;
use PhpSpec\ObjectBehavior;

class GildedRoseSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith([]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(GildedRose::class);
    }

    function it_updates_qualities_of_items()
    {
        $item = new Item('name', 10, 11);
        $this->beConstructedWith([$item]);
        $this->updateQuality();
        expect($item->quality)->shouldBe(10);
        expect($item->sell_in)->shouldBe(9);
    }
}
