<?php

namespace spec\App;

use App\Item;
use PhpSpec\ObjectBehavior;

class ItemSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('name', 11, 12);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Item::class);
    }

    function it_converts_to_string()
    {
        $this->__toString()->shouldReturn('name, 11, 12');
    }
}
