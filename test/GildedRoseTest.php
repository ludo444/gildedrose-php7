<?php

namespace App;

use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    /**
     * @dataProvider normalItemProvider
     * @dataProvider agedBrieItemProvider
     * @dataProvider sulfurasItemProvider
     * @dataProvider backstagePassItemProvider
     * @dataProvider conjuredItemProvider
     */
    public function testItemUpdate(Item $item, int $expectedSellIn, int $expectedQuality)
    {
        $gildedRose = new GildedRose([$item]);
        $gildedRose->updateQuality();

        $this->assertEquals($item->sell_in, $expectedSellIn);
        $this->assertEquals($item->quality, $expectedQuality);
    }

    public function normalItemProvider()
    {
        return [
            [new Item("normal", 10, 10), 9, 9],
            [new Item("normal", 10, 0), 9, 0],
            [new Item("normal", 0, 10), -1, 8],
            [new Item("normal", 0, 1), -1, 0],
            [new Item("normal", 0, 0), -1, 0],
            [new Item("normal", -10, 10), -11, 8],
            [new Item("normal", -10, 1), -11, 0],
            [new Item("normal", -10, 0), -11, 0],
        ];
    }

    public function agedBrieItemProvider()
    {
        return [
            [new Item("Aged Brie", 10, 10), 9, 11],
            [new Item("Aged Brie", 10, 50), 9, 50],
            [new Item("Aged Brie", 0, 10), -1, 12],
            [new Item("Aged Brie", 0, 49), -1, 50],
            [new Item("Aged Brie", 0, 50), -1, 50],
            [new Item("Aged Brie", -10, 10), -11, 12],
            [new Item("Aged Brie", -10, 49), -11, 50],
            [new Item("Aged Brie", -10, 50), -11, 50],
        ];
    }

    public function sulfurasItemProvider()
    {
        return [
            [new Item("Sulfuras, Hand of Ragnaros", 10, 10), 10, 80],
            [new Item("Sulfuras, Hand of Ragnaros", 0, 50), 0, 80],
            [new Item("Sulfuras, Hand of Ragnaros", -10, 10), -10, 80],
        ];
    }

    public function backstagePassItemProvider()
    {
        return [
            [new Item("Backstage passes to a TAFKAL80ETC concert", 20, 10), 19, 11],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 20, 50), 19, 50],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10), 9, 12],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49), 9, 50],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 10, 50), 9, 50],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 5, 10), 4, 13],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 5, 48), 4, 50],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 5, 50), 4, 50],
            [new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10), -1, 0],
            [new Item("Backstage passes to a TAFKAL80ETC concert", -10, 10), -11, 0],
        ];
    }

    public function conjuredItemProvider()
    {
        return [
            [new Item("Conjured", 10, 10), 9, 8],
            [new Item("Conjured", 10, 0), 9, 0],
            [new Item("Conjured", 10, 0), 9, 0],
            [new Item("Conjured", 0, 10), -1, 6],
            [new Item("Conjured", 0, 1), -1, 0],
            [new Item("Conjured", 0, 0), -1, 0],
            [new Item("Conjured", -10, 10), -11, 6],
            [new Item("Conjured", -10, 1), -11, 0],
            [new Item("Conjured", -10, 0), -11, 0],
        ];
    }
}
