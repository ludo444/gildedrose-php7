GildedRose-PHP7
===============

The original kata can be found here: https://github.com/emilybache/GildedRose-Refactoring-Kata

Complete requirements: https://github.com/emilybache/GildedRose-Refactoring-Kata/tree/master/GildedRoseRequirements.txt

Although it would be more effective to use `static` methods and to not create instances of objects, this solution is
focusing on separating the class responsibilities in best way possible to utilize inheritance and increase readability
of code.

The PHPUnit test covers possibilities and edge cases for all item types. In addition, solution provides PHPspec tests, to
independently describe and test each class for correct behavior.

Requirements
------------
**PHP 7+:**

Installation instructions can be found at: https://www.php.net/manual/en/install.php

**Composer:**

Installation instructions can be found at: https://getcomposer.org/doc/00-intro.md

Getting Started
---------------
After cloning the repository, switch into the directory:
```
cd gildedrose-php7
```
Run the Composer installation to install dependencies:
```
composer install
```

Running Tests
-------------
To run PHPUnit tests, simply execute command:
```
bin/phpunit
```
To run PHPspec tests, use following command:
```
bin/phpspec run
```
