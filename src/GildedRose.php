<?php
declare(strict_types=1);

namespace App;

use App\Provider\ItemUpdaterProvider;

final class GildedRose
{
    /**
     * @var Item[]
     */
    private $items = [];

    /**
     * @var ItemUpdaterProvider
     */
    private $itemUpdateProvider;

    public function __construct($items)
    {
        $this->items = $items;
        $this->itemUpdateProvider = new ItemUpdaterProvider();
    }

    public function updateQuality()
    {
        foreach ($this->items as $item) {
            $this->itemUpdateProvider->getUpdater($item)->update($item);
        }
    }
}

