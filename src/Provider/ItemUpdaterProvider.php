<?php
declare(strict_types=1);

namespace App\Provider;

use App\Item;
use App\Updater\AgedBrieUpdater;
use App\Updater\BackstagePassUpdater;
use App\Updater\ConjuredUpdater;
use App\Updater\ItemUpdater;
use App\Updater\ItemUpdaterInterface;
use App\Updater\SulfurasUpdater;

class ItemUpdaterProvider
{
    /**
     * @var ItemUpdaterInterface[]
     */
    private $updaters;

    public function __construct()
    {
        $this->updaters = [
            new AgedBrieUpdater(),
            new BackstagePassUpdater(),
            new ConjuredUpdater(),
            new SulfurasUpdater(),
            new ItemUpdater(),
        ];
    }

    public function getUpdater(Item $item): ItemUpdaterInterface
    {
        /** @var ItemUpdaterInterface $updater */
        foreach ($this->updaters as $updater) {
            if ($updater::supportsItem($item)) {
                return $updater;
            }
        }
        throw new \Exception("No updater found for item with name {$item->name}");
    }
}