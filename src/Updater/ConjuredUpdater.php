<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

class ConjuredUpdater extends ItemUpdater
{
    public static function supportsItem(Item $item): bool
    {
        return $item->name === 'Conjured';
    }

    public function updateQuality(Item $item)
    {
        $item->quality -= ($item->sell_in > 0 ? 2 : 4);
    }
}