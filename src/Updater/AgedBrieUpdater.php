<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

class AgedBrieUpdater extends ItemUpdater
{
    public static function supportsItem(Item $item): bool
    {
        return $item->name === 'Aged Brie';
    }

    protected function updateQuality(Item $item)
    {
        $item->quality += ($item->sell_in > 0 ? 1 : 2);
    }
}