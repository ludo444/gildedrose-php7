<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

interface ItemUpdaterInterface
{
    public function update(Item $item);

    public static function supportsItem(Item $item): bool;
}