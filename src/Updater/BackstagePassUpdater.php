<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

class BackstagePassUpdater extends ItemUpdater
{
    public static function supportsItem(Item $item): bool
    {
        return $item->name === 'Backstage passes to a TAFKAL80ETC concert';
    }

    protected function updateQuality(Item $item)
    {
        if ($item->sell_in <= 0) {
            $item->quality = 0;
        } elseif ($item->sell_in <= 5) {
            $item->quality += 3;
        } elseif ($item->sell_in <= 10) {
            $item->quality += 2;
        } else {
            $item->quality += 1;
        }
    }
}