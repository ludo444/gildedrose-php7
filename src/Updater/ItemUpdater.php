<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

class ItemUpdater implements ItemUpdaterInterface
{
    public function update(Item $item)
    {
        $item->sell_in -= 1;
        $this->updateQuality($item);
        if ($item->quality < 0) {
            $item->quality = 0;
        } elseif ($item->quality > 50) {
            $item->quality = 50;
        }
    }

    public static function supportsItem(Item $item): bool
    {
        return true;
    }

    protected function updateQuality(Item $item)
    {
        $item->quality -= ($item->sell_in > 0 ? 1 : 2);
    }
}