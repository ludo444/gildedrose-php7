<?php
declare(strict_types=1);

namespace App\Updater;

use App\Item;

class SulfurasUpdater implements ItemUpdaterInterface
{
    public function update(Item $item)
    {
        $item->quality = 80;
    }

    public static function supportsItem(Item $item): bool
    {
        return $item->name === 'Sulfuras, Hand of Ragnaros';
    }
}